
import './App.css';

// UserContext
import {UserProvider} from './UserContext.js';

// when the dev wants to render multiple components inside a resuable function such as App, Fragment component of react is used to enclose these components to avaoid errors in our frontend application; the pair of <> and </> can also be used pero hindi ito tinatanggap ng ibang browsers since walang laman, so gamitin na lang yung Fragment
import {useEffect, useState} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';


// Web Components
import AppNavbar from './AppNavbar';

// Web Pages
import Home from "./Pages/Home";
import Courses from "./Pages/Courses";
import CourseView from './components/CourseView';
import Register from "./Pages/Register";
import Login from "./Pages/Login";
import Logout from "./Pages/Logout";
import Error from "./Pages/Error";
// jsx are similar to html tags with one major difference- that is being able to apply javascript code
// react-router-dom = allows us to simulate changing pages in rect. y default, react is used for SPA (Single Page Application)
// Single Page Application
  // 

// bago gamitin si Route at Routes, need mo muna si Router. 
// Router (BrowseReouter) is used to wrap components that uses react-router-dom and allows the use of routes and the routing system. Not wrapping Routes and Route components will rende an error 
// Routes - holds all the Route components (used to wap Route elements that are meant to hold the endpoint and the component to be rendered in the page)

// Route - assigns an endpoint and displays the appropriate page component for that endpoint:
  // path - assigns the endpoint through the string data type
  // element attribute - assigns the page component to be rendered/displayed at that particular endpoint
function App() {

  const [user, setUser] = useState ({
    id: null,
    isAdmin: null
    });

  const unsetUser = ()=>{localStorage.clear();
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage)
  }, [user])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container fluid>
        <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path='/courses' element={<Courses/>}/>
          <Route path='/courses/:courseId' element={<CourseView/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/logout' element={<Logout/>}/>
          <Route path='/register' element={<Register/>}/>
          <Route path="*" element={<Error/>} />          
        </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;

