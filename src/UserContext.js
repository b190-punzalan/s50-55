import React from 'react';
// Creates UserContext object
/*
createContent method allows s to store a diff data inside an object, in this case called UserContext. This set of info can be shared to other components within the app. With this ethod, we will be able to create a global state
*/
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;
// export without default needs destructuring before being able to be imported by other components

export default UserContext;
