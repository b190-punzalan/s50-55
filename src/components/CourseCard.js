import {Card, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

/*export default function CourseCard(props){

	console.log(props);
	console.log(typeof props);

	return(
		<Card>
            <Card.Body>
                <Card.Title>{props.courseProp.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{props.courseProp.description}.</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{props.courseProp.price}</Card.Text>
                <Button variant="primary">Enroll</Button>
            </Card.Body>
        </Card>
	)
}
*/

// we can try to deconstruct the props object received from the parent component (Courses.js) so that the main object from the mock database will be accessed
export default function CourseCard({courseProp}){

	console.log(courseProp);
	console.log(typeof courseProp);

	// destructure the object courseProp imbes isusulat pa na courseProp.name etc:
	const {name, description, price, _id} = courseProp;

	// const [count, setCount]=useState(0);
	// const [seats, setSeats] = useState(10);
	// create another useState to efactor the enroll function
	// const [isOpen, setIsOpen] = useState(true);

// use the state hook for this component to be able to store its state. States are used to keep track of the info related to individual components. nag-aassign na tayo ng initial value as we try to render it in our webpage
	// function enroll(){
	// 	setCount(count+1);
	// 	console.log(`Enrollees: ${count}`);
	// 	setSeats(seats-1);
	// 	console.log(`Seats: ${seats}`);
	// };

// Solution to s51 activity:
	// function enroll(){
	// 	if (seats>0){
	// 		setCount(count+1);
	// 		console.log(`Enrollees: ${count}`);
	// 		setSeats(seats-1);
	// 		console.log(`Seats: ${seats}`);
	// 	}else{
	// 		alert("No more seats");
	// 	}
	// }

	// useEffect(()=>{
	// 	if(seats===0){
	// 		setIsOpen(false);
	// 	}
	// },[seats]);
	// useEffect requires 2 arguments: function and dependency array
		// function- to set which ines of codes are to be executed
		// dependency array-identifues which variables are to be listened to, in terms of changing the state, for the function to be executed
			// if the array is left e,pty, the codes will not listen to any variable but executed only once
	return(
		<Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}.</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                
                <Link className='btn btn-primary' to={`/courses/${_id}`}>Details</Link>
           
            </Card.Body>
        </Card>
	)
}