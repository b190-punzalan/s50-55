import {Fragment, useEffect, useState} from 'react';

import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';

// the course in our CourseCard component can be sent as a prop
// prop is shorthand of "property" since the components are considered as object in ReactJS
	// the curly braces are used for props to signify that we are providing info using JS expressions rather than hard coded values which use double quotes - pag mock database curly braces na lang gamitin
export default function Courses(){
	// console.log(coursesData);

	const [courses, setCourses] = useState([]);

	useEffect(()=>{fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			setCourses(data.map(course=>{
				return(
					<CourseCard key={course.id} courseProp={course}/>
					);
			}));
		})		
	},[])

	/*the map method loops through the individual course objects and returns components for each course*/

	/*const courses=coursesData.map(course=>{
		return (
			<CourseCard key={course.id} courseProp={course}/>
		)
	})*/
	
	return(
		<Fragment>
			{courses}
        </Fragment>
	)
}

