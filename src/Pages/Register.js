import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

// sweetalert2
import Swal from 'sweetalert2';


export default function Register(){

	const { user } = useContext(UserContext);


	// state hooks to store the values of the input fields
	const [email, setEmail]= useState('');
	const [password1, setPassword1]= useState('');
	const [password2, setPassword2]= useState('');
	// state to determine whether the submit button is enabled or not
	const[isActive, setIsActive]= useState(false);

	// to check if we have successfully mplemented te 2-way binding
	console.log(email);
	console.log(password1);
	console.log(password2);

	useEffect(()=>{
		if((email !=='' && password !== '')){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password]);

	function login(e){
		e.preventDefault();

		// fetch method now will communicate with our backend application providing it with a stringified JSON coming from the body
		// SYNTAX:
		/*
		fetch('url', {options})
		.then(res=>res.json())
		.then(data=>{})

		.then methods are used to create promise chain to catch the data from the fetch method

		*/
		fetch('http://localhost:4000/users/register',{
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data=>{console.log(data);
			if (typeof data.access!=='undefined'){
				localStorage.setItem('token',data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Registration successful!",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			}else{
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email."
				})
			}
		});

	/*useEffect(()=>{
		if((email !=='' && password1 !== '' && password2 !=='') && (password1===password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password1, password2]);

	function registerUser(e){
		e.preventDefault();

// clear input fields
/*
setter functions can also bring back the states of the getter variables if the dev wants to
*/
// 	setEmail('');
// 	setPassword1('');
// 	setPassword2('');

// 		alert('Thank you for registering!');
// 	}

// */
	return (
		(user.email !== null) ?
		<Navigate to='/courses' />
		:
	    <Form onSubmit={(e)=>registerUser(e)}>
	      <Form.Group controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        	type="email" 
	        	placeholder="Enter email" 
	        	value={email}
	        	onChange={e=>setEmail(e.target.value)}
	        	required
	        />
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Password"
	        	value={password1}
	        	onChange={e=>setPassword1(e.target.value)}
	        	required
	        	/>
	      </Form.Group>

	      <Form.Group controlId="password2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Password"
	        	value={password2}
	        	onChange={e=>setPassword2(e.target.value)}
	        	required
	        	/>
	      </Form.Group>
	      {isActive ?
	      <Button variant="primary" type="submit" id="submitBtn">
	        Submit
	      </Button>
	      :
	      <Button variant="primary" type="submit" id="submitBtn" disabled>
	        Submit
	      </Button>
	  		}
	    </Form>
	  );

}